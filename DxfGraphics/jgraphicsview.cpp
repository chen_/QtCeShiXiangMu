﻿#include "jgraphicsview.h"
#include <QDebug>
#include <QCoreApplication>

JGraphicsView::JGraphicsView(QWidget *parent)
    : QGraphicsView(parent)
    , zoomScale(250)
{

}

void JGraphicsView::zoomIn(int level)
{
    zoomScale += level;
    setupMatrix();
}

void JGraphicsView::zoomOut(int level)
{
    zoomScale -= level;
    setupMatrix();
}


void JGraphicsView::setupMatrix()
{
    qreal d_scale = qPow(qreal(2), (zoomScale - 250) / qreal(50));

    QMatrix matrix;
    matrix.scale(d_scale, d_scale);

    setMatrix(matrix);
}

void JGraphicsView::wheelEvent(QWheelEvent *event)
{
    QPoint scrollAmount = event->angleDelta();
    // 正值表示滚轮远离使用者（放大），负值表示朝向使用者（缩小）
    scrollAmount.y() > 0 ? zoomIn(6) : zoomOut(6);
    QCoreApplication::processEvents();
}
