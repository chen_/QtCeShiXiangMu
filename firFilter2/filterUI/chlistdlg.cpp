#include "chlistdlg.h"
#include "ui_chlistdlg.h"

ChListDlg::ChListDlg(QMap<int, QString> chInfo, QWidget *parent) :
    QDialog(parent),
    index(-1),
    ui(new Ui::ChListDlg)
{
    ui->setupUi(this);

    ui->tableWidget->horizontalHeader()->setStretchLastSection(true);
    int row = 0;
    ui->tableWidget->setRowCount(chInfo.size());
    for(auto it = chInfo.begin(); it != chInfo.end(); ++it)
    {
        QTableWidgetItem *item = new QTableWidgetItem(QString::number(it.key()));
        item->setFlags(item->flags() & (~Qt::ItemIsEditable));
        ui->tableWidget->setItem(row, 0, item);

        item = new QTableWidgetItem(it.value());
        item->setFlags(item->flags() & (~Qt::ItemIsEditable));
        ui->tableWidget->setItem(row, 1, item);

        ++row;
    }
}

ChListDlg::~ChListDlg()
{
    delete ui;
}

void ChListDlg::show()
{
    exec();
}

void ChListDlg::on_tableWidget_doubleClicked(const QModelIndex &ind)
{
    index = ind.row();
    accept();
}
