﻿#include "singlechaanle.h"
#include "ui_singlechaanle.h"

SingleChaanle::SingleChaanle(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::SingleChaanle),
    RubBand(new QRubberBand(QRubberBand::Rectangle, this)),
    Xup(0),
    Xdown(500),
    Yup(100),
    Ydown(-100)
{
    ui->setupUi(this);
    Init();
}

SingleChaanle::~SingleChaanle()
{
    RubBand->deleteLater();
    delete ui;
}

void SingleChaanle::Init()
{
    QSharedPointer<QCPAxisTicker> yTicker(new QCPAxisTicker);
    yTicker->setTickCount(4);
    ui->Plot->yAxis->setTicker(yTicker);
    ui->Plot->xAxis->setRange(Xup, Xdown);
    ui->Plot->yAxis->setRange(Yup, Ydown);
    ui->Plot->addGraph();
    ui->Plot->graph(0)->setPen(QPen(Qt::blue));
    QVector<double> x(500), y(500);
    for (auto i = 0; i < 500; i++) {
        x[i] = i;
        y[i] = rand() % 50;
    }
    ui->Plot->graph(0)->setData(x, y);
    ui->Plot->addGraph();
    ui->Plot->graph(1)->setPen(QPen(Qt::red));
    ui->Plot->setContextMenuPolicy(Qt::CustomContextMenu);
    connect(ui->Plot, SIGNAL(mousePress(QMouseEvent*)), this, SLOT(mousePressEvent(QMouseEvent*)));
    connect(ui->Plot, SIGNAL(mouseMove(QMouseEvent*)), this, SLOT(mouseMoveEvent(QMouseEvent*)));
    connect(ui->Plot, SIGNAL(mouseRelease(QMouseEvent*)), this, SLOT(mouseReleaseEvent(QMouseEvent*)));
    connect(ui->Plot, SIGNAL(selectionChangedByUser()), this, SLOT(selectionChanged()));
    ui->Plot->setSelectionTolerance(1);
    ui->Plot->selectionRect()->setPen(QPen(Qt::red));
    // 需要选中曲线时，QCP::iSelectPlottables必须要加上
    ui->Plot->setInteractions(QCP::iSelectAxes | QCP::iSelectLegend | QCP::iSelectPlottables);
    ui->Plot->rescaleAxes();
    ui->Plot->replot();
    this->setLayout(ui->hLayoutMain);
    ui->hLayoutMain->setStretch(0, 2);
    ui->hLayoutMain->setStretch(1, 87);
    ui->hLayoutMain->setStretch(2, 10);
    ui->WaveInfo->setText("this");
}

QString SingleChaanle::GetWaveInfo(const ChWaveInfo& Info)
{
    return QString("Ch:%1\nFreq:%2\nTime:%3\nValue:%4").
            arg(Info.ChNumber).
            arg(Info.Frequency).
            arg(Info.SampTime).
            arg(Info.Value);
}

void SingleChaanle::on_Plot_customContextMenuRequested(const QPoint &pos)
{
    qDebug() << pos;
}

void SingleChaanle::mousePressEvent(QMouseEvent *e)
{
    if(!ui->Plot->viewport().contains(e->pos())) return;
    switch (e->button()) {
    case Qt::LeftButton:
    {
        if(!RubBand->isVisible()) {
            int padding = ui->cBoxWave->width() +
                    ui->Plot->yAxis->padding() +
                    ui->Plot->yAxis->offset() +
                    ui->Plot->yAxis->tickLabelPadding();
            StartPoint.setX(e->pos().x() + padding);
            // Y轴全部
            StartPoint.setY(0);
            RubBand->setGeometry(QRect(StartPoint, QSize()));
            RubBand->show();
        }
        if (ui->Plot->xAxis->selectedParts().testFlag(QCPAxis::spAxis))
            ui->Plot->axisRect()->setRangeDrag(ui->Plot->xAxis->orientation());
        else if (ui->Plot->yAxis->selectedParts().testFlag(QCPAxis::spAxis))
            ui->Plot->axisRect()->setRangeDrag(ui->Plot->yAxis->orientation());
        else
            ui->Plot->axisRect()->setRangeDrag(Qt::Horizontal|Qt::Vertical);
    }
        break;
    case Qt::MidButton:
    {
        ui->Plot->xAxis->setRange(0, 500);
//        ui->Plot->rescaleAxes();
        ui->Plot->replot();
    }
        break;
    case Qt::RightButton:
    {
        RubBand->hide();
    }
        break;
    default:
        break;
    }
}

void SingleChaanle::mouseMoveEvent(QMouseEvent *e)
{
    if(!ui->Plot->viewport().contains(e->pos())) return;
    if(RubBand->isVisible()) {
        QPoint EndPoint;
        int padding = ui->cBoxWave->width() +
                ui->Plot->yAxis->padding() +
                ui->Plot->yAxis->offset() +
                ui->Plot->yAxis->tickLabelPadding();
        EndPoint.setX(e->pos().x() + padding);
        EndPoint.setY(ui->Plot->height());
        RubBand->setGeometry(QRect(StartPoint, EndPoint).normalized());
    }
}

void SingleChaanle::mouseReleaseEvent(QMouseEvent *e)
{
    if(!ui->Plot->viewport().contains(e->pos())) return;
    if(e->button() == Qt::LeftButton) {
        const QRect zoomRect = RubBand->geometry();
        int xp1, yp1, xp2, yp2;
        zoomRect.getCoords(&xp1, &yp1, &xp2, &yp2);
        int padding = ui->cBoxWave->width() +
                ui->Plot->yAxis->padding() +
                ui->Plot->yAxis->offset() +
                ui->Plot->yAxis->tickLabelPadding();
        double x1 = static_cast<int>(ui->Plot->xAxis->pixelToCoord(xp1 - padding) + static_cast<double>(0.5f));
        double x2 = static_cast<int>(ui->Plot->xAxis->pixelToCoord(xp2 - padding) + static_cast<double>(0.5f));
        if(x2 - x1 > 1) {
            // 放大选中区域波形，改变x轴范围
            ui->Plot->xAxis->setRange(x1, x2);
        }
        else {
            // 到时线
            QVector<double> x(2), y(2);
            QRect view = ui->Plot->viewport();
//            QCPAxisRect* size = ui->Plot->axisRect();
            x[0] = x1;
            x[1] = x1;
            y[0] = -(view.height() / 2);
            y[1] = view.height() / 2;
            ui->Plot->graph(1)->setData(x, y);
            WaveInfo.SampTime = static_cast<float>(x[0]);
            WaveInfo.Value = ui->Plot->graph(0)->data()->at(static_cast<int>(x[0]))->mainValue();
            ui->WaveInfo->setText(GetWaveInfo(WaveInfo));
        }
        ui->Plot->replot();
        RubBand->hide();
    }
}

void SingleChaanle::selectionChanged()
{
    /*
     normally, axis base line, axis tick labels and axis labels are selectable separately, but we want
     the user only to be able to select the axis as a whole, so we tie the selected states of the tick labels
     and the axis base line together. However, the axis label shall be selectable individually.

     The selection state of the left and right axes shall be synchronized as well as the state of the
     bottom and top axes.

     Further, we want to synchronize the selection of the graphs with the selection state of the respective
     legend item belonging to that graph. So the user can select a graph by either clicking on the graph itself
     or on its legend item.
    */

    // make top and bottom axes be selected synchronously, and handle axis and tick labels as one selectable object:
    if (ui->Plot->xAxis->selectedParts().testFlag(QCPAxis::spAxis) || ui->Plot->xAxis->selectedParts().testFlag(QCPAxis::spTickLabels) ||
        ui->Plot->xAxis2->selectedParts().testFlag(QCPAxis::spAxis) || ui->Plot->xAxis2->selectedParts().testFlag(QCPAxis::spTickLabels)) {
        ui->Plot->xAxis2->setSelectedParts(QCPAxis::spAxis|QCPAxis::spTickLabels);
        ui->Plot->xAxis->setSelectedParts(QCPAxis::spAxis|QCPAxis::spTickLabels);
    }
    // make left and right axes be selected synchronously, and handle axis and tick labels as one selectable object:
    if (ui->Plot->yAxis->selectedParts().testFlag(QCPAxis::spAxis) || ui->Plot->yAxis->selectedParts().testFlag(QCPAxis::spTickLabels) ||
        ui->Plot->yAxis2->selectedParts().testFlag(QCPAxis::spAxis) || ui->Plot->yAxis2->selectedParts().testFlag(QCPAxis::spTickLabels)) {
        ui->Plot->yAxis2->setSelectedParts(QCPAxis::spAxis|QCPAxis::spTickLabels);
        ui->Plot->yAxis->setSelectedParts(QCPAxis::spAxis|QCPAxis::spTickLabels);
    }

    // synchronize selection of graphs with selection of corresponding legend items:
    for (int i=0; i<ui->Plot->graphCount(); ++i) {
        QCPGraph *graph = ui->Plot->graph(i);
        QCPPlottableLegendItem *item = ui->Plot->legend->itemWithPlottable(graph);
        if (item->selected() || graph->selected()) {
            item->setSelected(true);
            graph->setSelection(QCPDataSelection(graph->data()->dataRange()));
        }
    }
}
