﻿#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QLabel>
#include <QPushButton>
#include <QComboBox>
#include <QCheckBox>
#include <QRadioButton>
#include <QToolButton>
#include <QScrollArea>
#include <QScrollBar>
#include <QHBoxLayout>
#include <QVBoxLayout>

QT_BEGIN_NAMESPACE
namespace Ui { class Widget; }
QT_END_NAMESPACE

class Widget : public QWidget
{
    Q_OBJECT

public:
    Widget(QWidget *parent = nullptr);
    ~Widget();

protected Q_SLOTS:
    void btnTest1_clicked();
    void btnTest2_clicked();
    void btnTest3_clicked();
    void btnTest4_clicked();

private:
    Ui::Widget *ui;

    // controls
    QPushButton *btnTest1;
    QPushButton *btnTest2;
    QPushButton *btnTest3;
    QPushButton *btnTest4;
    QPushButton *btnTest5;
    QPushButton *btnTest6;
    QPushButton *btnTest7;
    QPushButton *btnTest8;
    QVBoxLayout *vLayOperation;
    QFrame *sepLine;
    // account
    QLabel *labelAccount;
    QPushButton *btnweibo;
    QPushButton *btnweichat;
    QPushButton *btnQQ;
    QPushButton *btnDouban;
    QPushButton *btnYixin;
    QToolButton *btnBound;
    QHBoxLayout *hLayBound;
    QToolButton *btnPerInfo;
    QToolButton *btnFriend;
    QHBoxLayout *hLayPerson;
    QVBoxLayout *vLayAccount;
    QFrame *lineAccount;
    // general
    QLabel *labelGeneral;
    QLabel *labelFont;
    QComboBox *cBoxFont;
    QLabel *labelStart;
    QCheckBox *checkStart;
    QLabel *labelConnect;
    QCheckBox *checkConnect;
    QLabel *labelAnimate;
    QCheckBox *checkAnimate;
    QLabel *labelGPU;
    QCheckBox *checkGPU;
    QLabel *labelColse;
    QRadioButton *rbtnMin;
    QRadioButton *rbtnClose;
    QLabel *labelTimedColse;
    QCheckBox *checkTimedClose;
    QVBoxLayout *vLayGeneral;
    QFrame *lineGeneral;
    // play
    QLabel *labelPlay;
    QLabel *labelPlayList;
    QRadioButton *rbtnReplaceList;
    QRadioButton *rbtnAddList;
    QLabel *labelAutoPlay;
    QCheckBox *checkAutoPlay;
    QLabel *labelProgress;
    QCheckBox *checkProgress;
    QLabel *labelEffect;
    QCheckBox *checkEffect;
    QLabel *labelDevice;
    QComboBox *cBoxDevice;
    QVBoxLayout *vLayPlay;
    QFrame *linePlay;
    // message and privacy
    QLabel *labelMessage;
    QLabel *labelPrivacyMsg;
    QRadioButton *rbtnEveryone;
    QRadioButton *rbtnAttention;
    QLabel *labelNotify;
    QCheckBox *checkSongList;
    QCheckBox *checkFavour;
    QCheckBox *checkNew;
    QLabel *labelState;
    QCheckBox *checkState;
    QLabel *labelRank;
    QRadioButton *rbtnEveryoneSee;
    QRadioButton *rbtnAttentionSee;
    QRadioButton *rbtnOnlySelfSee;
    QVBoxLayout *vLayMessage;

    QVBoxLayout *vLayScrollArea;
    QWidget *widgetMain;
    QScrollArea *scrollArea;
    QHBoxLayout *hLayMain;
};
#endif // WIDGET_H
